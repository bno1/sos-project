#!/usr/bin/env python3

import logging
import argparse
import sys
import json
import itertools
import subprocess
import time
import numpy as np


LOG = logging.getLogger(__name__)


def gen_input():
    pop_rows = 5
    pop_cols = 6
    constr = 0.729
    w_min = 0.5
    w_max = 1.0
    max_iter = 500
    nsamples = 100
    std_dev = 0.35
    randwalk_step_scale = 0.3

    perlin_octaves = [
        (20, 0.7),
        (10, 0.3),
        (5, 0.15),
        (1, 0.15),
    ]

    dims = [2, 20]
    phis = [(2.05, 2.05), (1.85, 2.25), (2.25, 1.85)]
    functions = ['sphere', 'resenbrock', 'rastrigin', 'griewank']
    topology = ['fullgraph', 'ring', '4neighbours']
    variant = ['vanilla', 'inertia', 'constriction']
    rng = ['uniform', 'normal', 'randwalk', 'perlin']
    rng_broadcast = [True, False]

    it = itertools.product(
        dims,
        phis,
        functions,
        topology,
        variant,
        rng,
        rng_broadcast,
    )

    descriptions = []

    for (dim, (phi1, phi2), func, topo, var, rng, rng_broadcast) in it:
        descriptions.append(dict(
            dims=dim,
            pop_rows=pop_rows,
            pop_cols=pop_cols,
            phi1=phi1,
            phi2=phi2,
            std_dev=std_dev,
            constr=constr,
            w_min=w_min,
            w_max=w_max,
            rng_broadcast=rng_broadcast,
            max_iter=max_iter,
            nsamples=nsamples,
            function=func,
            topology=topo,
            variant=var,
            rng=rng,
            perlin_octaves=perlin_octaves,
            randwalk_step_scale=randwalk_step_scale
        ))

    with open('input.json', 'w') as f:
        json.dump(descriptions, f, indent=2)


def group_results_by(results, attribs):
    def keyfunc(r):
        descr = r['description']
        return tuple(descr[attrib] for attrib in attribs)

    results = sorted(results, key=keyfunc)
    for (k, r) in itertools.groupby(results, keyfunc):
        yield (k, list(r))


def gen_output():
    tables = []

    def fix(s):
        return s.replace('_', '\\_')

    with open('output.json', 'r') as f:
        results = json.load(f)

    for ((function, variant), results) in group_results_by(results, ('function', 'variant')):
        parts = []

        parts.append(r'''\begin{{table}}
\caption{{{function} -- {variant}}}
\begin{{center}}
\begin{{tabular}}{{|l|l|l|l|l|l|}}
    \hline
    $\phi_1, \phi_2$ & Topology & RNG & Mean Val, Std & Min Val & Abs Error \\
'''.format(function=fix(function), variant=fix(variant)))

        best_score = float('inf')

        for r in results:
            best_s = min(r['samples'])
            r['best'] = best_s
            r['mean_score'] = np.mean([s for s in r['samples']])
            r['std'] = np.std([s for s in r['samples']])

            # best_score = min(best_s, best_score)
            best_score = min(r['mean_score'], best_score)

        for ((phi1, phi2), results) in group_results_by(results, ('phi1', 'phi2')):
            parts.append(r'''    \hline
''')

            for ((rng, rng_broadcast), results) in group_results_by(results, ('rng','rng_broadcast')):
                for ((topology,), results) in group_results_by(results, ('topology',)):
                    for r in results:
                        best_s = r['best']
                        mean_s = r['mean_score']
                        std_s = r['std']
                        pf = r'\bfseries\boldmath' if mean_s == best_score else ''

                        rng_name = rng + '_bcast' if rng_broadcast else rng

                        parts.append(
r'''    {pf} ${}, {}$ & {pf} {} & {pf} {} & {pf} {:.2e}, {:.2e} & {pf} {:.2e} & {pf} {:.2e} \\
'''.format(phi1, phi2, fix(topology), fix(rng_name), mean_s, std_s, best_s,
           abs(mean_s - best_score), pf=pf))

        parts.append(r'''    \hline
\end{tabular}
\end{center}
\end{table}
''')

        tables.append(''.join(parts))

    with open('pdf/results.tex', 'w') as f:
        f.write('\n\n'.join(tables))


def main(command, debug=False):
    do_gen = False
    do_run = False
    do_output = False

    if command == 'all':
        do_gen = True
        do_run = True
        do_output = True
    elif command == 'gen':
        do_gen = True
    elif command == 'run':
        do_run = True
    elif command == 'out':
        do_output = True
    else:
        LOG.critical('Unknown command %s', command)
        return 1

    if do_gen:
        gen_input()

    if do_run:
        extra_args = []
        if not debug:
            extra_args.append('--release')

        subprocess.check_call(['cargo', 'build'] + extra_args)

        t = time.perf_counter()
        subprocess.check_call(['cargo', 'run'] + extra_args)
        dt = time.perf_counter() - t

        print('Took %s seconds' % dt)

    if do_output:
        gen_output()

    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(dest='command')

    cmd_gen_input = subparsers.add_parser('gen', help='generate an input.json')

    cmd_run = subparsers.add_parser(
        'run', help='run the project'
    )
    cmd_run.add_argument(
        '--debug', action='store_true', help='run the debug binary'
    )

    cmd_out = subparsers.add_parser(
        'out', help='generate latex file'
    )

    cmd_all = subparsers.add_parser(
        'all', help='generate input, run the project and generate latex file'
    )
    cmd_all.add_argument(
        '--debug', action='store_true', help='run the debug binary'
    )

    args = parser.parse_args()
    sys.exit(main(**vars(args)))
