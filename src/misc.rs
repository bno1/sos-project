#[inline]
pub fn argmin_iter<I, T>(mut iter: I) -> Option<(T, usize)>
where
    T: PartialOrd<T>,
    I: Iterator<Item = T>,
{
    if let Some(m) = iter.next() {
        let mut minimum = m;
        let mut min_idx = 0;

        for (i, v) in iter.enumerate() {
            if v < minimum {
                minimum = v;
                min_idx = i + 1;
            }
        }

        Some((minimum, min_idx))
    } else {
        None
    }
}
/*
pub fn min_iter_pair<I, U, V>(mut iter: I) -> Option<(U, V)>
    where V: PartialOrd<V>,
          I: Iterator<Item = (U, V)>
{
    if let Some(m) = iter.next() {
        let mut minimum = m;

        for p in iter {
            if p.1 < minimum.1 {
                minimum = p;
            }
        }

        Some(minimum)
    } else {
        None
    }
}
*/
