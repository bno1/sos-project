use num_traits;

use std::iter::FromIterator;
use std::marker::PhantomData;
use std::ops::{AddAssign, Index, IndexMut, MulAssign};
use std::slice::SliceIndex;
use std::fmt;
use serde::{ser, de};
use serde::ser::SerializeSeq;

pub struct Rows<'a, T> {
    chunks: std::slice::Chunks<'a, T>,
}

impl<'a, T> Iterator for Rows<'a, T> {
    type Item = Vector<T, RefData<'a, T>>;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.chunks.next().map(|v| Vector {
            __phantom_t__: PhantomData,
            data: RefData { data: v },
        })
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.chunks.size_hint()
    }

    #[inline]
    fn count(self) -> usize {
        self.chunks.count()
    }
}

impl<'a, T> ExactSizeIterator for Rows<'a, T> {}
//impl<'a, T> FusedIterator for Rows<'a, T> {}

pub struct RowsMut<'a, T> {
    chunks: std::slice::ChunksMut<'a, T>,
}

impl<'a, T> Iterator for RowsMut<'a, T> {
    type Item = Vector<T, MutRefData<'a, T>>;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.chunks.next().map(|v| Vector {
            __phantom_t__: PhantomData,
            data: MutRefData { data: v },
        })
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.chunks.size_hint()
    }

    #[inline]
    fn count(self) -> usize {
        self.chunks.count()
    }
}

impl<'a, T> ExactSizeIterator for RowsMut<'a, T> {}
//impl<'a, T> FusedIterator for RowsMut<'a, T> {}

pub struct ColIter<'a, T> {
    i: usize,
    step: usize,
    data: &'a [T],
}

impl<'a, T> ColIter<'a, T> {
    #[inline]
    fn count_ref(&self) -> usize {
        if self.i < self.data.len() {
            (self.data.len() - self.i) / self.step + 1
        } else {
            0
        }
    }
}

impl<'a, T> Iterator for ColIter<'a, T> {
    type Item = &'a T;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        if self.i < self.data.len() {
            let v = &self.data[self.i];
            self.i += self.step;

            Some(v)
        } else {
            None
        }
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.count_ref();

        (size, Some(size))
    }

    #[inline]
    fn count(self) -> usize {
        self.count_ref()
    }
}

impl<'a, T> ExactSizeIterator for ColIter<'a, T> {}
//impl<'a, T> FusedIterator for RowsMut<'a, T> {}

pub struct ColMutIter<'a, T> {
    iter: std::iter::StepBy<std::slice::IterMut<'a, T>>,
}

impl<'a, T> Iterator for ColMutIter<'a, T> {
    type Item = &'a mut T;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }

    #[inline]
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.iter.size_hint()
    }

    #[inline]
    fn count(self) -> usize {
        self.iter.count()
    }
}

impl<'a, T> ExactSizeIterator for ColMutIter<'a, T> {}
//impl<'a, T> FusedIterator for RowsMut<'a, T> {}

pub struct OwnedData<T> {
    data: Vec<T>,
}

pub struct RefData<'a, T> {
    data: &'a [T],
}

pub struct MutRefData<'a, T> {
    data: &'a mut [T],
}

pub trait Storage<T> {
    fn len(&self) -> usize;
    fn as_slice(&self) -> &[T];
    fn iter(&self) -> std::slice::Iter<T>;
    fn chunks(&self, size: usize) -> std::slice::Chunks<T>;

    fn idx<I>(&self, i: I) -> &I::Output
    where
        I: SliceIndex<[T]>;
}

pub trait MutStorage<T>: Storage<T> {
    fn as_slice_mut(&mut self) -> &mut [T];
    fn iter_mut(&mut self) -> std::slice::IterMut<T>;
    fn chunks_mut(&mut self, size: usize) -> std::slice::ChunksMut<T>;
    fn fill(&mut self, val: T)
    where
        T: Clone;

    fn idx_mut<I>(&mut self, i: I) -> &mut I::Output
    where
        I: SliceIndex<[T]>;
}

pub trait OwnedStorage<T>: Storage<T> + MutStorage<T> {
    fn from_elem(nelems: usize, val: T) -> Self
    where
        T: Clone;

    fn from_slice(data: &[T]) -> Self
    where
        T: Clone;

    fn from_vec(data: Vec<T>) -> Self;
}

impl<T> Storage<T> for OwnedData<T> {
    #[inline]
    fn len(&self) -> usize {
        self.data.len()
    }

    #[inline]
    fn as_slice(&self) -> &[T] {
        self.data.as_slice()
    }

    #[inline]
    fn iter(&self) -> std::slice::Iter<T> {
        self.data.iter()
    }

    #[inline]
    fn chunks(&self, size: usize) -> std::slice::Chunks<T> {
        self.data.chunks(size)
    }

    #[inline]
    fn idx<I>(&self, i: I) -> &I::Output
    where
        I: SliceIndex<[T]>,
    {
        self.data.index(i)
    }
}

impl<T> MutStorage<T> for OwnedData<T> {
    #[inline]
    fn as_slice_mut(&mut self) -> &mut [T] {
        self.data.as_mut_slice()
    }

    #[inline]
    fn iter_mut(&mut self) -> std::slice::IterMut<T> {
        self.data.iter_mut()
    }

    #[inline]
    fn chunks_mut(&mut self, size: usize) -> std::slice::ChunksMut<T> {
        self.data.chunks_mut(size)
    }

    #[inline]
    fn fill(&mut self, val: T)
    where
        T: Clone,
    {
        self.data.iter_mut().for_each(|x| *x = val.clone());
    }

    #[inline]
    fn idx_mut<I>(&mut self, i: I) -> &mut I::Output
    where
        I: SliceIndex<[T]>,
    {
        self.data.index_mut(i)
    }
}

impl<T> OwnedStorage<T> for OwnedData<T> {
    #[inline]
    fn from_elem(nelems: usize, val: T) -> Self
    where
        T: Clone,
    {
        let mut data = Vec::with_capacity(nelems);
        data.resize(nelems, val);

        Self { data }
    }

    #[inline]
    fn from_slice(data: &[T]) -> Self
    where
        T: Clone,
    {
        Self {
            data: Vec::from(data),
        }
    }

    #[inline]
    fn from_vec(data: Vec<T>) -> Self {
        Self { data }
    }
}

impl<'a, T> Storage<T> for RefData<'a, T> {
    #[inline]
    fn len(&self) -> usize {
        self.data.len()
    }

    #[inline]
    fn as_slice(&self) -> &[T] {
        self.data
    }

    #[inline]
    fn iter(&self) -> std::slice::Iter<T> {
        self.data.iter()
    }

    #[inline]
    fn chunks(&self, size: usize) -> std::slice::Chunks<T> {
        self.data.chunks(size)
    }

    #[inline]
    fn idx<I>(&self, i: I) -> &I::Output
    where
        I: SliceIndex<[T]>,
    {
        self.data.index(i)
    }
}

impl<'a, T> From<&'a [T]> for RefData<'a, T> {
    #[inline]
    fn from(data: &[T]) -> RefData<'_, T> {
        RefData { data }
    }
}

impl<'a, T> Storage<T> for MutRefData<'a, T> {
    #[inline]
    fn len(&self) -> usize {
        self.data.len()
    }

    #[inline]
    fn as_slice(&self) -> &[T] {
        self.data
    }

    #[inline]
    fn iter(&self) -> std::slice::Iter<T> {
        self.data.iter()
    }

    #[inline]
    fn chunks(&self, size: usize) -> std::slice::Chunks<T> {
        self.data.chunks(size)
    }

    #[inline]
    fn idx<I>(&self, i: I) -> &I::Output
    where
        I: SliceIndex<[T]>,
    {
        self.data.index(i)
    }
}

impl<'a, T> MutStorage<T> for MutRefData<'a, T> {
    #[inline]
    fn as_slice_mut(&mut self) -> &mut [T] {
        self.data
    }

    #[inline]
    fn iter_mut(&mut self) -> std::slice::IterMut<T> {
        self.data.iter_mut()
    }

    #[inline]
    fn chunks_mut(&mut self, size: usize) -> std::slice::ChunksMut<T> {
        self.data.chunks_mut(size)
    }

    #[inline]
    fn fill(&mut self, val: T)
    where
        T: Clone,
    {
        self.data.iter_mut().for_each(|x| *x = val.clone());
    }

    #[inline]
    fn idx_mut<I>(&mut self, i: I) -> &mut I::Output
    where
        I: SliceIndex<[T]>,
    {
        self.data.index_mut(i)
    }
}

impl<'a, T> From<&'a mut [T]> for MutRefData<'a, T> {
    #[inline]
    fn from(data: &mut [T]) -> MutRefData<'_, T> {
        MutRefData { data }
    }
}

pub struct Matrix<T, S>
where
    S: Storage<T>,
{
    __phantom_t__: PhantomData<T>,
    nrows: usize,
    ncols: usize,
    data: S,
}

pub type MatrixOwned<T> = Matrix<T, OwnedData<T>>;
pub type MatrixRef<'a, T> = Matrix<T, RefData<'a, T>>;
pub type MatrixMutRef<'a, T> = Matrix<T, MutRefData<'a, T>>;

pub struct Vector<T, S>
where
    S: Storage<T>,
{
    __phantom_t__: PhantomData<T>,
    data: S,
}

pub type VectorOwned<T> = Vector<T, OwnedData<T>>;
pub type VectorRef<'a, T> = Vector<T, RefData<'a, T>>;
pub type VectorMutRef<'a, T> = Vector<T, MutRefData<'a, T>>;

impl<T, S> Matrix<T, S>
where
    S: Storage<T>,
{
    #[inline]
    pub fn zeros(nrows: usize, ncols: usize) -> Self
    where
        S: OwnedStorage<T>,
        T: num_traits::Zero + Clone,
    {
        Self::from_elem(nrows, ncols, T::zero())
    }

    #[inline]
    pub fn from_elem(nrows: usize, ncols: usize, val: T) -> Self
    where
        S: OwnedStorage<T>,
        T: Clone,
    {
        let data = S::from_elem(nrows * ncols, val);

        Matrix {
            __phantom_t__: PhantomData,
            nrows,
            ncols,
            data,
        }
    }

    #[inline]
    pub fn from_slice(nrows: usize, ncols: usize, data: &[T]) -> Option<Self>
    where
        S: OwnedStorage<T>,
        T: Clone,
    {
        let real_size = nrows * ncols;

        if real_size <= data.len() {
            Some(Matrix {
                __phantom_t__: PhantomData,
                nrows,
                ncols,
                data: S::from_slice(&data[0..real_size]),
            })
        } else {
            None
        }
    }

    #[inline]
    pub fn view_from_slice<'a>(nrows: usize, ncols: usize, data: &'a [T]) -> Option<Self>
    where
        S: Storage<T> + From<&'a [T]>,
    {
        let real_size = nrows * ncols;

        if real_size <= data.len() {
            Some(Matrix {
                __phantom_t__: PhantomData,
                nrows,
                ncols,
                data: S::from(&data[0..real_size]),
            })
        } else {
            None
        }
    }

    #[inline]
    pub fn view_from_slice_mut<'a>(nrows: usize, ncols: usize, data: &'a mut [T]) -> Option<Self>
    where
        S: MutStorage<T> + From<&'a mut [T]>,
    {
        let real_size = nrows * ncols;

        if real_size <= data.len() {
            Some(Matrix {
                __phantom_t__: PhantomData,
                nrows,
                ncols,
                data: S::from(&mut data[0..real_size]),
            })
        } else {
            None
        }
    }

    #[inline]
    pub fn nrows(&self) -> usize {
        self.nrows
    }

    #[inline]
    pub fn ncols(&self) -> usize {
        self.ncols
    }

    #[inline]
    pub fn size(&self) -> (usize, usize) {
        (self.nrows, self.ncols)
    }

    #[inline]
    pub fn iter(&self) -> std::slice::Iter<T> {
        self.data.iter()
    }

    #[inline]
    pub fn iter_mut(&mut self) -> std::slice::IterMut<T>
    where
        S: MutStorage<T>,
    {
        self.data.iter_mut()
    }

    #[inline]
    pub fn rows(&self) -> Rows<T> {
        Rows {
            chunks: self.data.chunks(self.ncols),
        }
    }

    #[inline]
    pub fn rows_mut(&mut self) -> RowsMut<T>
    where
        S: MutStorage<T>,
    {
        RowsMut {
            chunks: self.data.chunks_mut(self.ncols),
        }
    }

    #[inline]
    pub fn row(&self, i: usize) -> Vector<T, RefData<'_, T>> {
        let p = i * self.ncols;

        Vector {
            __phantom_t__: PhantomData,
            data: RefData {
                data: self.data.idx(p..p + self.ncols),
            },
        }
    }

    #[inline]
    pub fn row_mut(&mut self, i: usize) -> Vector<T, MutRefData<'_, T>>
    where
        S: MutStorage<T>,
    {
        let p = i * self.ncols;

        Vector {
            __phantom_t__: PhantomData,
            data: MutRefData {
                data: self.data.idx_mut(p..p + self.ncols),
            },
        }
    }

    #[inline]
    pub fn col_iter(&self, i: usize) -> ColIter<T> {
        debug_assert!(i < self.ncols);

        ColIter {
            i,
            step: self.ncols,
            data: self.data.as_slice(),
        }
    }

    #[inline]
    pub fn col_iter_mut(&mut self, i: usize) -> ColMutIter<T>
    where
        S: MutStorage<T>,
    {
        debug_assert!(i < self.ncols);

        ColMutIter {
            iter: self.data.as_slice_mut()[i..].iter_mut().step_by(self.ncols),
        }
    }

    #[inline]
    pub fn fill(&mut self, v: T)
    where
        S: MutStorage<T>,
        T: Clone,
    {
        self.data.fill(v)
    }

    #[inline]
    pub fn assign<SR>(&mut self, m: &Matrix<T, SR>)
    where
        S: MutStorage<T>,
        SR: Storage<T>,
        T: Clone,
    {
        debug_assert!(self.ncols == m.ncols);

        if m.nrows == 1 {
            self.rows_mut().for_each(|mut row| {
                row.iter_mut()
                    .zip(m.iter())
                    .for_each(|(a, b)| *a = b.clone())
            });
        } else {
            assert!(self.nrows == m.nrows);

            self.iter_mut()
                .zip(m.iter())
                .for_each(|(a, b)| *a = b.clone());
        }
    }

    #[inline]
    pub fn col_assign<SR>(&mut self, i: usize, m: &Matrix<T, SR>, j: usize)
    where
        S: MutStorage<T>,
        SR: Storage<T>,
        T: Clone,
    {
        debug_assert!(self.ncols == m.ncols && i < self.ncols && j < m.ncols);

        self.col_iter_mut(i).zip(m.col_iter(j)).for_each(|(a, b)| *a = b.clone());
    }

    #[inline]
    pub fn split_rows(&self, row: usize) -> (MatrixRef<'_, T>, MatrixRef<'_, T>) {
        let row = row + 1;

        let (a, b) = self.data.as_slice().split_at(row * self.ncols);

        (
            Matrix::view_from_slice(row, self.ncols, a).unwrap(),
            Matrix::view_from_slice(self.nrows - row, self.ncols, b).unwrap(),
        )
    }

    #[inline]
    pub fn split_rows_mut(&mut self, row: usize) -> (MatrixMutRef<'_, T>, MatrixMutRef<'_, T>)
    where
        S: MutStorage<T>,
    {
        let row = row + 1;

        let (a, b) = self.data.as_slice_mut().split_at_mut(row * self.ncols);

        (
            Matrix::view_from_slice_mut(row, self.ncols, a).unwrap(),
            Matrix::view_from_slice_mut(self.nrows - row, self.ncols, b).unwrap(),
        )
    }
}

impl<T, S> MulAssign<T> for Matrix<T, S>
where
    S: MutStorage<T>,
    T: MulAssign<T> + Clone,
{
    #[inline]
    fn mul_assign(&mut self, val: T) {
        self.iter_mut().for_each(|v| *v *= val.clone());
    }
}

impl<T, S, SR> AddAssign<&Matrix<T, SR>> for Matrix<T, S>
where
    S: MutStorage<T>,
    SR: MutStorage<T>,
    T: AddAssign<T> + Clone,
{
    #[inline]
    fn add_assign(&mut self, m: &Matrix<T, SR>) {
        debug_assert!(self.size() == m.size());

        self.iter_mut()
            .zip(m.iter())
            .for_each(|(a, b)| *a += b.clone());
    }
}

impl<T, S> Vector<T, S>
where
    S: Storage<T>,
{
    #[inline]
    pub fn zeros(nelems: usize) -> Self
    where
        S: OwnedStorage<T>,
        T: num_traits::Zero + Clone,
    {
        Self::from_elem(nelems, T::zero())
    }

    #[inline]
    pub fn from_elem(nelems: usize, val: T) -> Self
    where
        S: OwnedStorage<T>,
        T: Clone,
    {
        let data = S::from_elem(nelems, val);

        Vector {
            __phantom_t__: PhantomData,
            data,
        }
    }

    #[inline]
    pub fn from_slice(data: &[T]) -> Self
    where
        S: OwnedStorage<T>,
        T: Clone,
    {
        Vector {
            __phantom_t__: PhantomData,
            data: S::from_slice(data),
        }
    }

    #[inline]
    pub fn from_ifunc<F>(nelems: usize, f: F) -> Self
    where
        S: OwnedStorage<T>,
        F: Fn(usize) -> T,
    {
        let mut data = Vec::with_capacity(nelems);
        for i in 0..nelems {
            data.push(f(i))
        }

        Vector {
            __phantom_t__: PhantomData,
            data: S::from_vec(data),
        }
    }

    #[inline]
    pub fn len(&self) -> usize {
        self.data.len()
    }

    #[inline]
    pub fn as_slice(&self) -> &[T] {
        self.data.as_slice()
    }

    #[inline]
    pub fn iter(&self) -> std::slice::Iter<T> {
        self.data.iter()
    }

    #[inline]
    pub fn iter_mut(&mut self) -> std::slice::IterMut<T>
    where
        S: MutStorage<T>,
    {
        self.data.iter_mut()
    }

    #[inline]
    pub fn fill(&mut self, v: T)
    where
        S: MutStorage<T>,
        T: Clone,
    {
        self.data.fill(v)
    }

    #[inline]
    pub fn fill_ifunc<F>(&mut self, f: F)
    where
        S: MutStorage<T>,
        F: Fn(usize) -> T,
    {
        let data = self.data.as_slice_mut();

        for i in 0..data.len() {
            data[i] = f(i);
        }
    }
}

impl<T, I, S> Index<I> for Vector<T, S>
where
    I: SliceIndex<[T]>,
    S: Storage<T>,
{
    type Output = I::Output;

    #[inline]
    fn index(&self, index: I) -> &Self::Output {
        self.data.idx(index)
    }
}

impl<T, I, S> IndexMut<I> for Vector<T, S>
where
    I: SliceIndex<[T]>,
    S: MutStorage<T>,
{
    #[inline]
    fn index_mut(&mut self, index: I) -> &mut Self::Output {
        self.data.idx_mut(index)
    }
}

impl<T, S> AddAssign<T> for Vector<T, S>
where
    S: MutStorage<T>,
    T: AddAssign<T> + Clone,
{
    #[inline]
    fn add_assign(&mut self, val: T) {
        self.iter_mut().for_each(|v| *v += val.clone());
    }
}

impl<T, S> FromIterator<T> for Vector<T, S>
where
    S: OwnedStorage<T>,
{
    #[inline]
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = T>,
    {
        let data = Vec::from_iter(iter);

        Vector {
            __phantom_t__: PhantomData,
            data: S::from_vec(data),
        }
    }
}

impl<'a, T, S> FromIterator<&'a T> for Vector<T, S>
where
    S: OwnedStorage<T>,
    T: 'a + Clone,
{
    #[inline]
    fn from_iter<I>(iter: I) -> Self
    where
        I: IntoIterator<Item = &'a T>,
    {
        let data = Vec::from_iter(iter.into_iter().cloned());

        Vector {
            __phantom_t__: PhantomData,
            data: S::from_vec(data),
        }
    }
}

impl<T, S> fmt::Debug for Vector<T, S>
where
    S: Storage<T>,
    T: fmt::Debug,
{
    #[inline]
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Vector({:?})", self.as_slice())
    }
}

impl<T, S> ser::Serialize for Vector<T, S>
where
    S: Storage<T>,
    T: ser::Serialize,
{
    #[inline]
    fn serialize<SR>(&self, serializer: SR) -> Result<SR::Ok, SR::Error>
    where
        SR: ser::Serializer,
    {
        let mut seq = serializer.serialize_seq(Some(self.len()))?;
        for e in self.iter() {
            seq.serialize_element(e)?;
        }
        seq.end()
    }
}

impl<'de, T, S> de::Deserialize<'de> for Vector<T, S>
where
    S: OwnedStorage<T>,
    T: de::Deserialize<'de>,
{
    #[inline]
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: de::Deserializer<'de>,
    {
        let data = de::Deserialize::deserialize(deserializer)?;
        Ok(Vector {
            __phantom_t__: PhantomData,
            data: OwnedStorage::from_vec(data),
        })
    }
}
