use crate::matrix::*;
use crate::rng::*;
use crate::types::*;
use crate::perlin::*;
use rand::distributions::{Distribution, Normal, Uniform};

#[inline]
fn sample_from<D>(x: &mut MatrixOwned<Real>, d: &D, rng: &mut MyRng, broadcast: bool)
where
    D: Distribution<Real>,
{
    if broadcast {
        let (mut head, mut tail) = x.split_rows_mut(0);

        head.iter_mut().for_each(|x| *x = d.sample(rng));
        tail.assign(&head);
    } else {
        x.iter_mut().for_each(|x| *x = d.sample(rng));
    }
}

pub trait PSRng {
    fn get_factors(&mut self, rng: &mut MyRng) -> (&MatrixOwned<Real>, &MatrixOwned<Real>);

    fn reset(&mut self, _rng: &mut MyRng) {

    }
}

struct UniformPSRng {
    broadcast: bool,
    d1: Uniform<Real>,
    d2: Uniform<Real>,
    rands: (MatrixOwned<Real>, MatrixOwned<Real>),
}

impl UniformPSRng {
    #[inline]
    fn new(dims: usize, count: usize, broadcast: bool, phi1: Real, phi2: Real) -> Self {
        UniformPSRng {
            broadcast,
            d1: Uniform::new_inclusive(0.0, phi1),
            d2: Uniform::new_inclusive(0.0, phi2),
            rands: (
                MatrixOwned::zeros(dims, count),
                MatrixOwned::zeros(dims, count),
            ),
        }
    }
}

impl PSRng for UniformPSRng {
    #[inline]
    fn get_factors(&mut self, rng: &mut MyRng) -> (&MatrixOwned<Real>, &MatrixOwned<Real>) {
        sample_from(&mut self.rands.0, &self.d1, rng, self.broadcast);
        sample_from(&mut self.rands.1, &self.d2, rng, self.broadcast);

        (&self.rands.0, &self.rands.1)
    }
}

struct NormalPSRng {
    broadcast: bool,
    d1: Normal,
    d2: Normal,
    rands: (MatrixOwned<Real>, MatrixOwned<Real>),
}

impl NormalPSRng {
    #[inline]
    fn new(
        dims: usize,
        count: usize,
        broadcast: bool,
        phi1: Real,
        phi2: Real,
        std_dev: Real,
    ) -> Self {
        NormalPSRng {
            broadcast,
            d1: Normal::new(phi1 * 0.5, std_dev),
            d2: Normal::new(phi2 * 0.5, std_dev),
            rands: (
                MatrixOwned::zeros(dims, count),
                MatrixOwned::zeros(dims, count),
            ),
        }
    }
}

impl PSRng for NormalPSRng {
    #[inline]
    fn get_factors(&mut self, rng: &mut MyRng) -> (&MatrixOwned<Real>, &MatrixOwned<Real>) {
        sample_from(&mut self.rands.0, &self.d1, rng, self.broadcast);
        sample_from(&mut self.rands.1, &self.d2, rng, self.broadcast);

        (&self.rands.0, &self.rands.1)
    }
}

struct PerlinPSRng {
    broadcast: bool,
    noise_gen1: PerlinNoise,
    noise_gen2: PerlinNoise,
    rands: (MatrixOwned<Real>, MatrixOwned<Real>),
}

impl PerlinPSRng {
    #[inline]
    fn new<'a, I>(dims: usize, count: usize, broadcast: bool, phi1: Real, phi2: Real, octaves: I) -> Self
    where
        I: Iterator<Item = OctaveDescr>
    {
        let octaves: Vec<OctaveDescr> = octaves.collect();
        let amp_sum: Real = octaves.iter().map(|(_, amp)| amp).sum();

        let noise_dims = if broadcast { 1 } else { dims };

        PerlinPSRng {
            broadcast,
            noise_gen1: PerlinNoise::new(noise_dims, count, octaves.iter().map(|(per, amp)| (*per, amp * phi1 / amp_sum))),
            noise_gen2: PerlinNoise::new(noise_dims, count, octaves.iter().map(|(per, amp)| (*per, amp * phi2 / amp_sum))),
            rands: (MatrixOwned::zeros(dims, count), MatrixOwned::zeros(dims, count)),
        }
    }

    #[inline]
    fn sample_from(x: &mut MatrixOwned<Real>, noise: &mut PerlinNoise, rng: &mut MyRng, broadcast: bool) {
        if broadcast {
            let (mut head, mut tail) = x.split_rows_mut(0);

            noise.step(&mut head, rng);
            tail.assign(&head);
        } else {
            noise.step(x, rng);
        }
    }
}

impl PSRng for PerlinPSRng {
    #[inline]
    fn get_factors(&mut self, rng: &mut MyRng) -> (&MatrixOwned<Real>, &MatrixOwned<Real>) {
        Self::sample_from(&mut self.rands.0, &mut self.noise_gen1, rng, self.broadcast);
        Self::sample_from(&mut self.rands.1, &mut self.noise_gen2, rng, self.broadcast);

        (&self.rands.0, &self.rands.1)
    }

    fn reset(&mut self, rng: &mut MyRng) {
        self.noise_gen1.reset(rng);
        self.noise_gen2.reset(rng);
    }
}

struct RandomWalkPSRng {
    broadcast: bool,
    d1: Uniform<Real>,
    d2: Uniform<Real>,
    phi1: Real,
    phi2: Real,
    rands: (MatrixOwned<Real>, MatrixOwned<Real>),
}

impl RandomWalkPSRng {
    #[inline]
    fn new(dims: usize, count: usize, broadcast: bool, step_scale: Real, phi1: Real, phi2: Real) -> Self {
        RandomWalkPSRng {
            broadcast,
            d1: Uniform::new_inclusive(-phi1 * step_scale, phi1 * step_scale),
            d2: Uniform::new_inclusive(-phi2 * step_scale, phi2 * step_scale),
            //d1: Normal::new(0.0, phi1 * step_scale),
            //d2: Normal::new(0.0, phi2 * step_scale),
            phi1,
            phi2,
            rands: (
                MatrixOwned::zeros(dims, count),
                MatrixOwned::zeros(dims, count),
            ),
        }
    }

    #[inline]
    fn sample_from<D>(x: &mut MatrixOwned<Real>, d: &D, rng: &mut MyRng, broadcast: bool, max: Real)
    where
        D: Distribution<Real>
    {
        let randomwalk = |x: &mut Real| {
            let dx = d.sample(rng);
            let mut v = *x + dx;

            if v > max || v < 0.0 { v = *x - dx; }

            *x = v;
        };

        if broadcast {
            let (mut head, mut tail) = x.split_rows_mut(0);

            head.iter_mut().for_each(randomwalk);
            tail.assign(&head);
        } else {
            x.iter_mut().for_each(randomwalk);
        }
    }
}

impl PSRng for RandomWalkPSRng {
    #[inline]
    fn get_factors(&mut self, rng: &mut MyRng) -> (&MatrixOwned<Real>, &MatrixOwned<Real>) {
        Self::sample_from(&mut self.rands.0, &self.d1, rng, self.broadcast, self.phi1);
        Self::sample_from(&mut self.rands.1, &self.d2, rng, self.broadcast, self.phi2);

        (&self.rands.0, &self.rands.1)
    }

    #[inline]
    fn reset(&mut self, rng: &mut MyRng) {
        let distr = Uniform::new_inclusive(0.0, self.phi1);
        self.rands.0.iter_mut().for_each(|x| *x = distr.sample(rng));

        let distr = Uniform::new_inclusive(0.0, self.phi2);
        self.rands.1.iter_mut().for_each(|x| *x = distr.sample(rng));
    }
}

pub struct PSRngFactory {
    dims: usize,
    count: usize,
    broadcast: bool,
    phi1: Real,
    phi2: Real,
    step_scale: Real,
    std_dev: Real,
    octaves: Vec<OctaveDescr>,
}

impl PSRngFactory {
    #[inline]
    pub fn new() -> Self {
        PSRngFactory {
            dims: 0,
            count: 0,
            broadcast: true,
            phi1: REAL_NAN,
            phi2: REAL_NAN,
            step_scale: REAL_NAN,
            std_dev: REAL_NAN,
            octaves: Vec::new(),
        }
    }

    #[inline]
    pub fn set_dims(&mut self, dims: usize) {
        self.dims = dims;
    }

    #[inline]
    pub fn set_count(&mut self, count: usize) {
        self.count = count;
    }

    #[inline]
    pub fn set_broadcast(&mut self, broadcast: bool) {
        self.broadcast = broadcast;
    }

    #[inline]
    pub fn set_step_scale(&mut self, step_scale: Real) {
        self.step_scale = step_scale;
    }

    #[inline]
    pub fn set_phi(&mut self, phi1: Real, phi2: Real) {
        self.phi1 = phi1;
        self.phi2 = phi2;
    }

    #[inline]
    pub fn set_std_dev(&mut self, std_dev: Real) {
        self.std_dev = std_dev;
    }

    #[inline]
    pub fn set_octaves(&mut self, octaves: Vec<OctaveDescr>) {
        self.octaves = octaves;
    }

    #[inline]
    pub fn new_psrng(&self, name: &str) -> Option<Box<dyn PSRng + Send>> {
        match name {
            "uniform" => Some(Box::new(UniformPSRng::new(
                self.dims, self.count, self.broadcast, self.phi1, self.phi2,
            ))),
            "normal" => Some(Box::new(NormalPSRng::new(
                self.dims,
                self.count,
                self.broadcast,
                self.phi1,
                self.phi2,
                self.std_dev,
            ))),
            "perlin" => Some(Box::new(PerlinPSRng::new(
                self.dims,
                self.count,
                self.broadcast,
                self.phi1,
                self.phi2,
                self.octaves.iter().cloned(),
            ))),
            "randwalk" => Some(Box::new(RandomWalkPSRng::new(
                self.dims,
                self.count,
                self.broadcast,
                self.step_scale,
                self.phi1,
                self.phi2,
            ))),
            _ => None,
        }
    }
}
