use crate::landscape::*;
use crate::matrix::*;
use crate::types::*;
use std::ops::Range;

pub struct TestFunction {
    pub name: &'static str,
    pub minimum: (Real, Real),
    pub minimum_val: Real,
    pub range: Range<Real>,
    //pub fun: &'static ((Fn(Real, Real) -> Real) + Sync),
    pub fun_a:
        &'static (dyn Fn(&MatrixOwned<Real>, &mut VectorOwned<Real> /*, &mut MatrixOwned<Real>*/)
                      + Sync),
}

impl Landscape for TestFunction {
    #[inline]
    fn range(&self) -> &Range<Real> {
        &self.range
    }

    /*#[inline]
    fn apply(&self, x: Real, y: Real) -> Real {
        (self.fun)(x, y)
    }*/

    #[inline]
    fn apply_a(
        &self,
        x: &MatrixOwned<Real>,
        y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
    ) {
        (self.fun_a)(x, y /*, temp*/)
    }
}

#[inline]
pub fn sphere(x: Real, y: Real) -> Real {
    x * x + y * y
}

#[inline]
pub fn sphere_a_basic(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    for (y, x) in y.iter_mut().zip(x.row(0).iter().zip(x.row(1).iter())) {
        *y = sphere(*x.0, *x.1);
    }
}

#[inline]
pub fn sphere_a(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    y.fill(0.0);

    x.rows().for_each(|row| {
        y.iter_mut()
            .zip(row.iter())
            .for_each(|(yv, xv)| *yv += xv * xv)
    });
}

#[inline]
pub fn sphere_a1(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    y.fill(0.0);

    for row in x.rows() {
        for (yv, xv) in y.iter_mut().zip(row.iter()) {
            *yv += xv * xv;
        }
    }
}

#[inline]
pub fn rosenbrock(x: Real, y: Real) -> Real {
    const A: Real = 1.0;
    const B: Real = 10.0;

    (A - x).powi(2) + B * (y - x * x).powi(2)
}

#[inline]
pub fn rosenbrock_a_basic(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    for (y, x) in y.iter_mut().zip(x.row(0).iter().zip(x.row(1).iter())) {
        *y = rosenbrock(*x.0, *x.1)
    }
}

#[inline]
pub fn rosenbrock_a(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    const A: Real = 1.0;
    const B: Real = 10.0;

    y.fill(0.0);

    x.rows().zip(x.rows().skip(1)).for_each(|(row1, row2)| {
        y.iter_mut()
            .zip(row1.iter().zip(row2.iter()))
            .for_each(|(yv, (xv1, xv2))| *yv += (xv1 - A).powi(2) + B * (xv1 * xv1 - xv2).powi(2))
    });
}

#[inline]
pub fn rastrigin(x: Real, y: Real) -> Real {
    const A: Real = 10.0;

    A * 2.0 + x * x + y * y - A * ((TWO_PI * x).cos() + (TWO_PI * y).cos())
}

#[inline]
pub fn rastrigin_a_basic(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    for (y, x) in y.iter_mut().zip(x.row(0).iter().zip(x.row(1).iter())) {
        *y = rastrigin(*x.0, *x.1)
    }
}

#[inline]
pub fn rastrigin_a(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    const A: Real = 10.0;

    y.fill(A * (x.nrows() as Real));

    x.rows().for_each(|row| {
        y.iter_mut()
            .zip(row.iter())
            .for_each(|(yv, xv)| *yv += xv * xv - A * (TWO_PI * xv).cos())
    });
}

#[inline]
pub fn griewank(x: Real, y: Real) -> Real {
    1.0 + (1.0 / 4000.0) * (x * x + y * y) - x.cos() * (y * HALF_SQRT2).cos()
}

#[inline]
pub fn griewank_a_basic(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    for (y, x) in y.iter_mut().zip(x.row(0).iter().zip(x.row(1).iter())) {
        *y = griewank(*x.0, *x.1)
    }
}

#[inline]
pub fn griewank_a(
    x: &MatrixOwned<Real>,
    y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
) {
    let factor_iter = (1..(x.nrows() + 1)).map(|i| 1.0 / (i as Real).sqrt());

    y.fill(-1.0);

    x.rows().zip(factor_iter).for_each(|(row, ff)| {
        y.iter_mut().zip(row.iter()).for_each(|(a, b)| {
            *a *= (b * ff).cos();
        });
    });

    x.rows().for_each(|row| {
        y.iter_mut().zip(row.iter()).for_each(|(a, b)| {
            *a += b * b * (1.0 / 4000.0);
        });
    });

    *y += 1.0;
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand::random;

    fn test_a(
        x: &MatrixOwned<Real>,
        y: &mut VectorOwned<Real>,
        _temp: &mut MatrixOwned<Real>,
        f: &(dyn Fn(Real, Real) -> Real),
        f_a: &(dyn Fn(&MatrixOwned<Real>, &mut VectorOwned<Real> /*, &mut MatrixOwned<Real>*/)),
    ) {
        y.fill(-4851.0);

        f_a(x, y /*, temp*/);

        for (yv, x) in y.iter().zip(x.row(0).iter().zip(x.row(1).iter())) {
            assert!((f(*x.0, *x.1) - yv).abs() < 0.001);
        }
    }

    #[test]
    fn test_f_f_a() {
        let mut y = VectorOwned::zeros(100);
        let mut x = MatrixOwned::zeros(2, 100);
        x.iter_mut().for_each(|v| *v = random());
        let mut temp = MatrixOwned::zeros(x.nrows(), x.ncols());

        test_a(&x, &mut y, &mut temp, &sphere, &sphere_a_basic);
        test_a(&x, &mut y, &mut temp, &sphere, &sphere_a);
        test_a(&x, &mut y, &mut temp, &sphere, &sphere_a1);
        test_a(&x, &mut y, &mut temp, &rosenbrock, &rosenbrock_a_basic);
        test_a(&x, &mut y, &mut temp, &rosenbrock, &rosenbrock_a);
        test_a(&x, &mut y, &mut temp, &rastrigin, &rastrigin_a_basic);
        test_a(&x, &mut y, &mut temp, &rastrigin, &rastrigin_a);
        test_a(&x, &mut y, &mut temp, &griewank, &griewank_a_basic);
        test_a(&x, &mut y, &mut temp, &griewank, &griewank_a);
    }
}

pub const FUNCTIONS: [TestFunction; 4] = [
    TestFunction {
        name: "sphere",
        minimum: (0.0, 0.0),
        minimum_val: 0.0,
        range: -10.0..10.0,
        //fun: &sphere,
        fun_a: &sphere_a,
    },
    TestFunction {
        name: "resenbrock",
        minimum: (1.0, 1.0),
        minimum_val: 0.0,
        range: -2.0..2.0,
        //fun: &rosenbrock,
        fun_a: &rosenbrock_a,
    },
    TestFunction {
        name: "rastrigin",
        minimum: (0.0, 0.0),
        minimum_val: 0.0,
        range: -5.12..5.12,
        //fun: &rastrigin,
        fun_a: &rastrigin_a,
    },
    TestFunction {
        name: "griewank",
        minimum: (0.0, 0.0),
        minimum_val: 0.0,
        range: -10.0..10.0,
        //fun: &griewank,
        fun_a: &griewank_a,
    },
];

#[inline]
pub fn get_test_function(name: &str) -> Option<&'static TestFunction> {
    FUNCTIONS.iter().find(|f| f.name == name)
}
