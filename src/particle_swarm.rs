use crate::landscape::Landscape;
use crate::matrix::*;
use crate::misc::argmin_iter;
use crate::ps_data::ParticleData;
use crate::ps_rng::PSRng;
use crate::ps_topology::Topology;
use crate::ps_variant::Variant;
use crate::rng::*;
use crate::types::*;
use rand::distributions::Uniform;
use std::iter::FromIterator;

pub struct ProblemInstance<'a> {
    data: ParticleData,
    landscape: &'a (dyn Landscape + Send + Sync),
    topology: Box<dyn Topology + Send>,
    variant: Box<dyn Variant + Send>,
    psrng: Box<dyn PSRng + Send>,

    distr: Uniform<Real>,
}

impl<'a> ProblemInstance<'a> {
    #[inline]
    pub fn new(
        data: ParticleData,
        landscape: &'a (dyn Landscape + Send + Sync),
        topology: Box<dyn Topology + Send>,
        variant: Box<dyn Variant + Send>,
        psrng: Box<dyn PSRng + Send>,
    ) -> Self {
        let range = landscape.range();

        ProblemInstance {
            data,
            landscape,
            topology,
            variant,
            psrng,

            distr: Uniform::new_inclusive(range.start, range.end),
        }
    }

    #[inline]
    fn step(&mut self, rng: &mut MyRng) {
        let landscape = self.landscape;

        landscape.apply_a(
            &self.data.coords,
            &mut self.data.values, /*, &mut self.data.temp*/
        );

        for i in 0..self.data.count {
            if self.data.values[i] < self.data.pbest[i] {
                self.data.pbest[i] = self.data.values[i];
                self.data.pbest_coords.col_assign(i, &self.data.coords, i);
            }
        }

        self.topology.update(&self.data);

        self.variant.update_velocities(
            &mut self.data,
            self.topology.get_bests(),
            self.psrng.get_factors(rng),
        );

        self.variant.step();

        self.data.coords += &self.data.velocity;
    }

    #[inline]
    pub fn reset_and_run(&mut self, iterations: u32, rng: &mut MyRng) -> (VectorOwned<Real>, Real) {
        self.reset(rng);

        for _ in 0..iterations {
            self.step(rng);
        }

        debug_assert!((|| {
            let mut temp = VectorOwned::zeros(self.data.pbest.len());
            self.landscape.apply_a(&self.data.pbest_coords, &mut temp);
            temp.iter().zip(self.data.pbest.iter()).all(|(a, b)| (a - b).abs() < 0.0001)
        })());

        let (best, i) = argmin_iter(self.data.pbest.iter()).unwrap();

        (VectorOwned::from_iter(self.data.pbest_coords.col_iter(i)), *best)
    }

    #[inline]
    pub fn reset(&mut self, rng: &mut MyRng) {
        self.data.reset(&self.distr, rng);
        self.variant.reset();
        self.psrng.reset(rng);
    }
}
