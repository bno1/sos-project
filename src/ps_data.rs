use crate::matrix::*;
use crate::rng::*;
use crate::types::*;
use rand::distributions::{Distribution, Uniform};

pub struct ParticleData {
    pub dims: usize,
    pub count: usize,
    pub coords: MatrixOwned<Real>,
    pub values: VectorOwned<Real>,
    pub velocity: MatrixOwned<Real>,
    pub pbest: VectorOwned<Real>,
    pub pbest_coords: MatrixOwned<Real>,
    //pub temp: MatrixOwned<Real>
}

impl ParticleData {
    #[inline]
    pub fn new(dims: usize, count: usize) -> Self {
        ParticleData {
            dims,
            count,
            coords: MatrixOwned::zeros(dims, count),
            values: VectorOwned::zeros(count),
            velocity: MatrixOwned::zeros(dims, count),
            pbest: VectorOwned::zeros(count),
            pbest_coords: MatrixOwned::zeros(dims, count),
            //temp: MatrixOwned::zeros(2, count),
        }
    }

    #[inline]
    pub fn reset(&mut self, distr: &Uniform<Real>, rng: &mut MyRng) {
        self.coords.iter_mut().for_each(|x| *x = distr.sample(rng));

        self.values.fill(0.0);
        self.velocity.fill(0.0);
        self.pbest.fill(REAL_INF);
        self.pbest_coords.fill(REAL_NAN);
    }
}
