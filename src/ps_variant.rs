use crate::matrix::*;
use crate::ps_data::*;
use crate::types::*;

// Computes a += (b - c) * d element-wise
#[inline]
fn sub_mult<S1, S2, S3, S4>(
    a: &mut Matrix<Real, S1>,
    b: &Matrix<Real, S2>,
    c: &Matrix<Real, S3>,
    d: &Matrix<Real, S4>,
) where
    S1: MutStorage<Real>,
    S2: Storage<Real>,
    S3: Storage<Real>,
    S4: Storage<Real>,
{
    debug_assert!(a.size() == b.size());
    debug_assert!(a.size() == c.size());
    debug_assert!(a.size() == d.size());

    let it = a.iter_mut().zip(b.iter().zip(c.iter()).zip(d.iter()));

    for (av, ((bv, cv), dv)) in it {
        *av += (*bv - *cv) * *dv;
    }
}

pub trait Variant {
    fn get_name(&self) -> &str;
    fn update_velocities(
        &mut self,
        data: &mut ParticleData,
        lbests: &MatrixOwned<Real>,
        rands: (&MatrixOwned<Real>, &MatrixOwned<Real>),
    );
    fn step(&mut self) {}
    fn reset(&mut self) {}
}

pub struct Vanilla {}

impl Vanilla {
    #[inline]
    pub fn new() -> Self {
        Vanilla {}
    }
}

impl Variant for Vanilla {
    #[inline]
    fn get_name(&self) -> &str {
        "vanilla"
    }

    #[inline]
    fn update_velocities(
        &mut self,
        data: &mut ParticleData,
        lbests: &MatrixOwned<Real>,
        rands: (&MatrixOwned<Real>, &MatrixOwned<Real>),
    ) {
        sub_mult(
            &mut data.velocity,
            &data.pbest_coords,
            &data.coords,
            rands.0,
        );
        sub_mult(&mut data.velocity, lbests, &data.coords, rands.1);
    }
}

pub struct Intertia {
    w_max: Real,
    w_min: Real,
    max_iter: u32,
    iter: u32,
}

impl Intertia {
    #[inline]
    pub fn new(w_max: Real, w_min: Real, max_iter: u32) -> Self {
        Intertia {
            w_max,
            w_min,
            max_iter: max_iter - 1,
            iter: 0,
        }
    }
}

impl Variant for Intertia {
    #[inline]
    fn get_name(&self) -> &str {
        "inertia"
    }

    #[inline]
    fn update_velocities(
        &mut self,
        data: &mut ParticleData,
        lbests: &MatrixOwned<Real>,
        rands: (&MatrixOwned<Real>, &MatrixOwned<Real>),
    ) {
        let w = self.w_max - Real::from(self.iter) / Real::from(self.max_iter) * (self.w_max - self.w_min);

        data.velocity *= w;
        sub_mult(&mut data.velocity, &data.pbest_coords, &data.coords, rands.0);
        sub_mult(&mut data.velocity, lbests, &data.coords, rands.1);
    }

    fn step(&mut self) {
        self.iter = (self.iter + 1).min(self.max_iter);
    }

    fn reset(&mut self) {
        self.iter = 0;
    }
}

pub struct Constriction {
    constr: Real,
}

impl Constriction {
    #[inline]
    pub fn new(constr: Real) -> Self {
        Constriction { constr }
    }
}

impl Variant for Constriction {
    #[inline]
    fn get_name(&self) -> &str {
        "constriction"
    }

    #[inline]
    fn update_velocities(
        &mut self,
        data: &mut ParticleData,
        lbests: &MatrixOwned<Real>,
        rands: (&MatrixOwned<Real>, &MatrixOwned<Real>),
    ) {
        sub_mult(&mut data.velocity, &data.pbest_coords, &data.coords, rands.0);
        sub_mult(&mut data.velocity, lbests, &data.coords, rands.1);
        data.velocity *= self.constr
    }
}

pub struct VariantFactory {
    w_max: Real,
    w_min: Real,
    max_iter: u32,
    constr: Real,
}

impl VariantFactory {
    #[inline]
    pub fn new() -> Self {
        VariantFactory {
            w_max: REAL_NAN,
            w_min: REAL_NAN,
            max_iter: 0,
            constr: REAL_NAN,
        }
    }

    #[inline]
    pub fn set_intertia(&mut self, w_min: Real, w_max: Real) {
        self.w_max = w_max;
        self.w_min = w_min;
    }

    #[inline]
    pub fn set_max_iter(&mut self, max_iter: u32) {
        self.max_iter = max_iter;
    }

    #[inline]
    pub fn set_constr(&mut self, constr: Real) {
        self.constr = constr;
    }

    #[inline]
    pub fn new_variant(&self, name: &str) -> Option<Box<dyn Variant + Send>> {
        match name {
            "vanilla" => Some(Box::new(Vanilla::new())),
            "inertia" => Some(Box::new(Intertia::new(self.w_max, self.w_min, self.max_iter))),
            "constriction" => Some(Box::new(Constriction::new(self.constr))),
            _ => None,
        }
    }
}
