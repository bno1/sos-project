pub type Real = f64;
pub type Coord = (Real, Real);

pub const REAL_INF: Real = std::f64::INFINITY;
pub const REAL_NAN: Real = std::f64::NAN;
pub const TWO_PI: Real = std::f64::consts::PI * 2.0;
pub const HALF_SQRT2: Real = std::f64::consts::SQRT_2 * 0.5;
