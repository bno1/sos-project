use crate::matrix::*;
use crate::types::*;
use std::ops::Range;

pub trait Landscape {
    fn range(&self) -> &Range<Real>;
    //fn apply(&self, x: Real, y: Real) -> Real;
    fn apply_a(
        &self,
        x: &MatrixOwned<Real>,
        y: &mut VectorOwned<Real>, /*, temp: &mut MatrixOwned<Real>*/
    );
    //fn apply_c(&self, coord: Coord) -> Real;
}
