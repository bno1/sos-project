use rand::prelude::*;
use rand::rngs::mock::StepRng;

type InternalRng = SmallRng;

pub struct MyRng {
    rng: InternalRng,
}

impl MyRng {
    #[inline]
    pub fn from_rand_rng(source: impl Rng) -> Result<Self, rand::Error> {
        let rng = InternalRng::from_rng(source)?;

        Ok(MyRng { rng })
    }

    #[inline]
    pub fn split(&mut self) -> Self {
        let mut seed: <InternalRng as SeedableRng>::Seed = Default::default();

        self.fill_bytes(seed.as_mut());

        MyRng {
            rng: InternalRng::from_seed(seed),
        }
    }

    #[inline]
    pub fn from_thread() -> Result<Self, rand::Error> {
        MyRng::from_rand_rng(thread_rng())
    }

    #[inline]
    pub fn from_step(initial: u64, increment: u64) -> Result<Self, rand::Error> {
        MyRng::from_rand_rng(StepRng::new(initial, increment))
    }
}

impl RngCore for MyRng {
    #[inline]
    fn next_u32(&mut self) -> u32 {
        self.rng.next_u32()
    }

    fn next_u64(&mut self) -> u64 {
        self.rng.next_u64()
    }

    fn fill_bytes(&mut self, dest: &mut [u8]) {
        self.rng.fill_bytes(dest)
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand::Error> {
        self.rng.try_fill_bytes(dest)
    }
}
