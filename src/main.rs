use pso_lib::constants::*;
use pso_lib::particle_swarm::*;
use pso_lib::ps_data::*;
use pso_lib::ps_rng::PSRngFactory;
use pso_lib::ps_topology::TopologyFactory;
use pso_lib::ps_variant::VariantFactory;
use pso_lib::rng::*;
use pso_lib::test_functions::get_test_function;
use pso_lib::types::*;
use serde_derive::{Deserialize, Serialize};
use serde_json;
use std::fs;
use std::io;
use std::sync::mpsc;
use std::thread;
use std::vec::Vec;
use rayon::ThreadPoolBuilder;

#[derive(Debug, Clone, Serialize, Deserialize)]
struct ProblemDescription {
    dims: usize,
    pop_rows: usize,
    pop_cols: usize,
    phi1: Real,
    phi2: Real,
    std_dev: Real,
    constr: Real,
    w_min: Real,
    w_max: Real,
    rng_broadcast: bool,
    max_iter: u32,
    nsamples: u32,
    function: String,
    topology: String,
    variant: String,
    rng: String,
    perlin_octaves: Vec<(u32, Real)>,
    randwalk_step_scale: Real,
}

#[derive(Debug, Serialize, Deserialize)]
struct ProblemResults {
    description: ProblemDescription,
    samples: Vec<Real>,
}

#[inline]
fn run_simulations(pds: Vec<ProblemDescription>, rng: &mut MyRng) -> Vec<ProblemResults> {
    let (tx_r, rx_r) = mpsc::sync_channel(2 * NTHREADS);

    let pds_cnt = pds.len();

    let mut c_rng = rng.split();
    let producer_t = thread::spawn(move || {
        let pool = ThreadPoolBuilder::new()
            .num_threads(NTHREADS)
            .build()
            .unwrap();

        for pd in pds {
            let mut rng = c_rng.split();

            let tx = tx_r.clone();

            pool.spawn(move || {
                let mut topo_fact = TopologyFactory::new();
                let mut var_fact = VariantFactory::new();
                let mut psrng_fact = PSRngFactory::new();

                topo_fact.set_dims(pd.dims);
                topo_fact.set_pop(pd.pop_rows, pd.pop_cols);
                var_fact.set_max_iter(pd.max_iter);
                var_fact.set_intertia(pd.w_min, pd.w_max);
                var_fact.set_constr(pd.constr);
                psrng_fact.set_phi(pd.phi1, pd.phi2);
                psrng_fact.set_std_dev(pd.std_dev);
                psrng_fact.set_count(pd.pop_rows * pd.pop_cols);
                psrng_fact.set_dims(pd.dims);
                psrng_fact.set_broadcast(pd.rng_broadcast);
                psrng_fact.set_octaves(pd.perlin_octaves.clone());
                psrng_fact.set_step_scale(pd.randwalk_step_scale);

                let data = ParticleData::new(pd.dims, pd.pop_rows * pd.pop_cols);
                let topology = match topo_fact.new_topo(&pd.topology) {
                    Some(t) => t,
                    None => panic!("Unknown topology {}", pd.topology),
                };
                let variant = match var_fact.new_variant(&pd.variant) {
                    Some(v) => v,
                    None => panic!("Unknown variant {}", pd.variant),
                };
                let psrng = match psrng_fact.new_psrng(&pd.rng) {
                    Some(p) => p,
                    None => panic!("Unknown psrng {}", pd.rng),
                };

                let mut pi = ProblemInstance::new(
                    data,
                    get_test_function(&pd.function).unwrap(),
                    topology,
                    variant,
                    psrng,
                );

                let samples = (0..pd.nsamples)
                    .map(|_| pi.reset_and_run(pd.max_iter, &mut rng).1)
                    .collect();

                tx.send(ProblemResults {
                    description: pd,
                    samples,
                })
                .unwrap();
            });
        }
    });

    let mut results: Vec<ProblemResults> = Vec::with_capacity(pds_cnt);

    for pr in rx_r.iter() {
        results.push(pr);
    }

    producer_t.join().unwrap();

    results
}

fn main() -> io::Result<()> {
    let input = fs::read("input.json")?;

    let descriptions: Vec<ProblemDescription> = serde_json::from_slice(input.as_slice())
        .map_err(|e| io::Error::new(io::ErrorKind::InvalidInput, e))?;

    let mut rng = MyRng::from_thread().unwrap();
    //let mut rng = MyRng::from_step(0, 1).unwrap();

    let results = run_simulations(descriptions, &mut rng);

    let output = serde_json::to_vec_pretty(&results)
        .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e))?;
    fs::write("output.json", output)
}
