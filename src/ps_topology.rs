use crate::matrix::*;
use crate::misc::argmin_iter;
use crate::ps_data::*;
use crate::types::*;

pub trait Topology {
    fn update(&mut self, data: &ParticleData);
    fn get_bests(&self) -> &MatrixOwned<Real>;
}

pub struct FullGraph {
    best: MatrixOwned<Real>,
}

impl FullGraph {
    #[inline]
    pub fn new(dims: usize, count: usize) -> Self {
        FullGraph {
            best: MatrixOwned::from_elem(dims, count, REAL_NAN),
        }
    }
}

impl Topology for FullGraph {
    #[inline]
    fn update(&mut self, data: &ParticleData) {
        let (_, min_idx) = argmin_iter(data.pbest.iter()).unwrap();

        for (mut row, v) in self.best.rows_mut().zip(data.pbest_coords.col_iter(min_idx)) {
            row.fill(*v);
        }
    }

    #[inline]
    fn get_bests(&self) -> &MatrixOwned<Real> {
        &self.best
    }
}

pub struct Ring {
    best: MatrixOwned<Real>,
}

impl Ring {
    #[inline]
    pub fn new(dims: usize, count: usize) -> Self {
        Ring {
            best: MatrixOwned::from_elem(dims, count, REAL_NAN),
        }
    }
}

impl Topology for Ring {
    #[inline]
    fn update(&mut self, data: &ParticleData) {
        let count = self.best.ncols();

        assert!(data.pbest.len() == count);

        for i in 0..count {
            let mut j = i;

            for k in [i + count - 1, i + 1].iter().map(|v| v % count) {
                if data.pbest[k] < data.pbest[j] {
                    j = k;
                }
            }

            self.best.col_assign(i, &data.pbest_coords, j);
        }
    }

    #[inline]
    fn get_bests(&self) -> &MatrixOwned<Real> {
        &self.best
    }
}

pub struct FourNeighbours {
    rows: usize,
    columns: usize,
    best: MatrixOwned<Real>,
}

impl FourNeighbours {
    #[inline]
    pub fn new(dims: usize, rows: usize, columns: usize) -> Self {
        FourNeighbours {
            rows,
            columns,
            best: MatrixOwned::from_elem(dims, rows * columns, REAL_NAN),
        }
    }
}

impl Topology for FourNeighbours {
    #[inline]
    fn update(&mut self, data: &ParticleData) {
        for i in 0..self.rows {
            for j in 0..self.columns {
                let mut idx_best = i * self.columns + j;

                for ki in [i + self.rows - 1, i + 1].iter().map(|v| v % self.rows) {
                    let idx = ki * self.columns + j;

                    if data.pbest[idx] < data.pbest[idx_best] {
                        idx_best = idx;
                    }
                }

                for kj in [j + self.columns - 1, j + 1].iter().map(|v| v % self.columns) {
                    let idx = i * self.columns + kj;

                    if data.pbest[idx] < data.pbest[idx_best] {
                        idx_best = idx;
                    }
                }

                self.best.col_assign(i * self.columns + j, &data.pbest_coords, idx_best);
            }
        }
    }

    #[inline]
    fn get_bests(&self) -> &MatrixOwned<Real> {
        &self.best
    }
}

pub struct TopologyFactory {
    dims: usize,
    rows: usize,
    columns: usize,
}

impl TopologyFactory {
    #[inline]
    pub fn new() -> Self {
        TopologyFactory {
            dims: 0,
            rows: 0,
            columns: 0,
        }
    }

    #[inline]
    pub fn set_dims(&mut self, dims: usize) {
        self.dims = dims;
    }

    #[inline]
    pub fn set_pop(&mut self, rows: usize, columns: usize) {
        self.rows = rows;
        self.columns = columns;
    }

    #[inline]
    pub fn new_topo(&self, name: &str) -> Option<Box<dyn Topology + Send>> {
        match name {
            "fullgraph" => Some(Box::new(FullGraph::new(self.dims, self.rows * self.columns))),
            "ring" => Some(Box::new(Ring::new(self.dims, self.rows * self.columns))),
            "4neighbours" => Some(Box::new(FourNeighbours::new(self.dims, self.rows, self.columns))),
            _ => None,
        }
    }
}
