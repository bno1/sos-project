use crate::matrix::*;
use crate::types::*;
use crate::rng::MyRng;
use rand::distributions::{Distribution,Uniform};

pub type OctaveDescr = (u32, Real);

pub struct Octave {
    tick: u32,
    period: u32,
    data: MatrixOwned<Real>,
    dt: MatrixOwned<Real>,
    distr: Uniform<Real>,
}

impl Octave {
    #[inline]
    pub fn new(dims: usize, count: usize, descr: OctaveDescr) -> Self {
        let (period, amp) = descr;

        Octave {
            tick: 0,
            period,
            data: MatrixOwned::zeros(dims, count),
            dt: MatrixOwned::zeros(dims, count),
            distr: Uniform::new_inclusive(0.0, amp),
        }
    }

    #[inline]
    pub fn step(&mut self, rng: &mut MyRng) {
        if self.tick == 0 {
            self.tick = self.period;

            let period = Real::from(self.period);
            let distr = self.distr;

            self.dt.iter_mut().zip(self.data.iter()).for_each(|(d, x)| {
                *d = (distr.sample(rng) - x) / period
            });
        }

        self.data += &self.dt;
        self.tick -= 1;
    }

    #[inline]
    pub fn reset(&mut self, rng: &mut MyRng) {
        self.tick = 0;

        let distr = self.distr;
        self.data.iter_mut().for_each(|x| *x = distr.sample(rng));
        self.dt.fill(0.0);
    }
}

pub struct PerlinNoise {
    dims: usize,
    count: usize,
    octaves: Vec<Octave>,
}

impl PerlinNoise {
    #[inline]
    pub fn new<I>(dims: usize, count: usize, octaves: I) -> Self
    where
        I: Iterator<Item = OctaveDescr>
    {
        PerlinNoise {
            dims,
            count,
            octaves: octaves.map(|descr| Octave::new(dims, count, descr)).collect(),
        }
    }

    #[inline]
    pub fn step<S>(&mut self, out: &mut Matrix<Real, S>, rng: &mut MyRng)
    where
        S: MutStorage<Real>
    {
        debug_assert!(out.nrows() == self.dims && out.ncols() == self.count);

        out.fill(0.0);

        for oct in self.octaves.iter_mut() {
            *out += &oct.data;
            oct.step(rng);
        }
    }

    #[inline]
    pub fn reset(&mut self, rng: &mut MyRng) {
        for oct in self.octaves.iter_mut() {
            oct.reset(rng);
        }
    }
}
