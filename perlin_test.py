import numpy as np
import matplotlib.pyplot as plt


class Octave:
    def __init__(self, size, period, amp):
        self.tick = 0
        self.period = period
        self.amp = amp
        self.data = np.random.rand(*size) * self.amp
        self.dt = np.zeros(size)

    def step(self):
        if self.tick == 0:
            self.tick = self.period

            temp = np.random.rand(*self.data.shape) * self.amp
            self.dt = (temp - self.data) / self.period

        self.data += self.dt
        self.tick -= 1


class PerlinNoise:
    def __init__(self, size, octaves):
        self.size = size
        self.octaves = [Octave(size, period, amp) for (period, amp) in octaves]
        self.data = sum(o.data for o in self.octaves)

    def step(self):
        self.data.fill(0.0)

        for o in self.octaves:
            self.data += o.data
            o.step()


noise = PerlinNoise((1, ), [
    (20, 0.5),
    (10, 0.3),
    (5, 0.15),
    (1, 0.05),
])

x = []
ys = [[] for _ in range(noise.size[0])]

for i in range(100):
    x.append(i)

    for (v, y) in zip(noise.data, ys):
        y.append(v)

    noise.step()

plt.figure()
for y in ys:
    plt.plot(x, y)
plt.show()
