## Particle Swarm Optimization benchmark

- Multi-threaded
- Custom matrix code with good performance by exploiting compiter auto-vectorization

See `pres.odp` for more details.

Use `control.py` for running the project:

- `./control.py gen` - generates the `input.json` file describing all the test scenarios
- `./control.py run` - run the project on `input.json`, generating `output.json` with the results (takes 2-3 minutes on 4 cores)

The `Plots.ipynb` notebook is used for parsing results and generating graphs. You can use `poetry` to install dependnecies and open it: `poetry install` and `poetry run jupyter-lab`.
