use criterion::*;
use rand::random;
use std::time::Duration;

use pso_lib::matrix::*;
use pso_lib::types::*;

pub fn sphere(x: Real, y: Real) -> Real {
    x * x + y * y
}

pub fn sphere_nda_basic(
    x: ndarray::ArrayView2<Real>,
    y: ndarray::ArrayViewMut1<Real>,
    _temp: ndarray::ArrayViewMut2<Real>,
) {
    ndarray::Zip::from(y).and(x.gencolumns()).apply(|y, x| {
        *y = sphere(x[0], x[1]);
    });
}

pub fn sphere_nda(
    x: ndarray::ArrayView2<Real>,
    mut y: ndarray::ArrayViewMut1<Real>,
    _temp: ndarray::ArrayViewMut2<Real>,
) {
    y.fill(0.0);

    let ys = y.as_slice_mut().unwrap();

    x.genrows().into_iter().for_each(|row| {
        ys.iter_mut()
            .zip(row.as_slice().unwrap())
            .for_each(|(y, x)| *y += x * x);
    });
    /*ndarray::Zip::from(x.genrows()).apply(|row| {
        let rs = &row.as_slice().unwrap();

        for (rx, yx) in rs.iter().zip(ys.iter_mut()) {
            *yx += rx*rx;
        }
    });*/
}

pub fn sphere_nda1(
    x: ndarray::ArrayView2<Real>,
    mut y: ndarray::ArrayViewMut1<Real>,
    _temp: ndarray::ArrayViewMut2<Real>,
) {
    y.fill(0.0);

    ndarray::Zip::from(x.genrows()).apply(|row| {
        ndarray::Zip::from(y.view_mut()).and(&row).apply(|y, x| {
            *y += x * x;
        });
    });
}

fn test_nd_setup() -> (Vec<Real>, Vec<Real>) {
    let y = vec![0.0; 100];
    let x = (0..2 * y.len()).map(|_| random()).collect();

    return (x, y);
}

fn iter_sphere_func(
    c: &mut Criterion,
    name: &str,
    xx: &Vec<Real>,
    yy: &Vec<Real>,
    f: fn(ndarray::ArrayView2<Real>, ndarray::ArrayViewMut1<Real>, ndarray::ArrayViewMut2<Real>),
) {
    let cs = yy.len();
    let rs = xx.len() / cs;

    let x = ndarray::Array2::from_shape_vec((rs, cs), xx.clone()).unwrap();
    let mut y = ndarray::Array1::from_shape_vec(yy.len(), yy.clone()).unwrap();
    let mut temp = ndarray::Array2::zeros(x.dim());
    c.bench_function(name, move |b| {
        b.iter(|| {
            f(x.view(), y.view_mut(), temp.view_mut());
        })
    });
}

fn sphere_min_basic(x: &Vec<Real>, y: &mut Vec<Real>, _temp: &mut Vec<Real>) {
    unsafe {
        let v = x.as_slice();
        let r = y.as_mut_slice();
        let d = v.len() / r.len();

        for i in 0..r.len() {
            *r.get_unchecked_mut(i) = 0.0;
        }

        let mut j = 0;
        for _ in 0..d {
            for i in 0..r.len() {
                let t = v.get_unchecked(i + j);
                *r.get_unchecked_mut(i) += t * t;
            }

            j += r.len();
        }
    }
}

fn sphere_na_basic(x: &[Real], y: &mut [Real], _temp: &mut [Real]) {
    let rs = y.len();

    y.iter_mut().for_each(|v| *v = 0.0);

    x.chunks(rs).for_each(|chunk| {
        chunk
            .iter()
            .zip(y.iter_mut())
            .for_each(|(xv, yv)| *yv += xv * xv)
    });
}

fn sphere_mm_basic(x: &MatrixOwned<Real>, y: &mut VectorOwned<Real>, _temp: &mut MatrixOwned<Real>) {
    y.fill(0.0);

    x.rows().for_each(|row| {
        row.iter()
            .zip(y.iter_mut())
            .for_each(|(xv, yv)| *yv += xv * xv)
    });
}

fn sphere_bench(c: &mut Criterion) {
    let (xx, yy) = test_nd_setup();
    let ncols = yy.len();
    let nrows = xx.len() / ncols;

    iter_sphere_func(c, "sphere_nda_basic", &xx, &yy, sphere_nda_basic);
    iter_sphere_func(c, "sphere_nda", &xx, &yy, sphere_nda);
    iter_sphere_func(c, "sphere_nda1", &xx, &yy, sphere_nda1);

    /*
    let x = xx.clone();
    let mut y = yy.clone();
    let mut temp = xx.clone();
    c.bench_function("sphere_min_basic", move |b| b.iter(|| {
        sphere_min_basic(&x, &mut y, &mut temp);
    }));*/

    let x = xx.clone();
    let mut y = yy.clone();
    let mut temp = xx.clone();
    c.bench_function("sphere_na_basic", move |b| {
        b.iter(|| {
            sphere_na_basic(x.as_slice(), y.as_mut_slice(), temp.as_mut_slice());
        })
    });

    let x = MatrixOwned::from_slice(nrows, ncols, xx.as_slice()).unwrap();
    let mut y = VectorOwned::from_slice(yy.as_slice());
    let mut temp = MatrixOwned::zeros(nrows, ncols);
    c.bench_function("sphere_mm_basic", move |b| {
        b.iter(|| {
            sphere_mm_basic(&x, &mut y, &mut temp);
        })
    });
}

criterion_group! {
    name = benches;
    config = Criterion::default().measurement_time(Duration::from_millis(2000)).warm_up_time(Duration::from_millis(800));
    targets = sphere_bench
}
criterion_main!(benches);
